package com.example.myapplication.ui

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData

class GlobalVariables {

    companion object {

        var idUltimoCodigoBarrasInt = 9


        var fotos = mutableListOf<Bitmap>()

    }

}