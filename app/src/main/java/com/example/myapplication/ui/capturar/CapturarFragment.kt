package com.example.myapplication.ui.capturar

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.FragmentHomeBinding
import com.example.myapplication.ui.GlobalVariables
import com.karumi.dexter.BuildConfig
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
/*import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage*/
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Objects

class CapturarFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    var vFilename: String = ""

    var permissionCamera = mutableListOf(
        Manifest.permission.CAMERA

    )

    var permissionCameraOld = mutableListOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val capturarViewModel =
            ViewModelProvider(this).get(CapturarViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root



        binding.botonTomaFoto.setOnClickListener {

            takePhoto()
        }

        return root
    }


    private fun takePhoto(){




        Dexter.withContext(requireContext()).withPermissions(
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) permissionCamera else permissionCameraOld
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {



                if(multiplePermissionsReport.deniedPermissionResponses.size > 0)
                {

                    Toast.makeText(requireContext(), "Verifique los permis", Toast.LENGTH_LONG).show()
                    pidePermisos()
                }
                else
                {


                    /*val values = ContentValues()
                    values.put(MediaStore.Images.Media.TITLE, "New Picture")
                    values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")

                    //camera intent
                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                    // set filename


                    // set direcory folder
                    val file = getFile()
                    val image_uri = uriFromFile(file)


                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
                    startActivityForResult(cameraIntent, 100)*/
                }


            }


            override fun onPermissionRationaleShouldBeShown(
                list: List<PermissionRequest>,
                permissionToken: PermissionToken
            ) {


                Toast.makeText(requireContext(), "Verifique los permis", Toast.LENGTH_LONG).show()
                pidePermisos()
            }
        }).check()






    }

    fun getFile(): File
    {
        var folder: File? = null

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        vFilename = "FOTO_" + timeStamp + ".jpg"

        val dest: String = requireActivity().getExternalFilesDir(null).toString() + "/"
        folder = File(dest, "fotos")
        folder!!.mkdir()
        val f = File(folder, vFilename)

        return f
    }

    private fun uriFromFile(file: File): Uri? {
        return FileProvider.getUriForFile(
            Objects.requireNonNull<Context>(requireContext()),
            BuildConfig.LIBRARY_PACKAGE_NAME + ".provider", file)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {

            //File object of camera image
            val file = File("/sdcard/niabsen/", vFilename);


            //Uri of camera image

            var bitMap = BitmapFactory.decodeFile(file.absolutePath);

            addFoto(bitMap)
        }
    }

    fun addFoto(imagen: Bitmap)
    {
        if(GlobalVariables.fotos.size <= 0)
        {
            GlobalVariables.fotos.add(0, imagen)
        }
        else
        {
            if(GlobalVariables.fotos.size < 9)
            {
                GlobalVariables.fotos.add(
                    GlobalVariables.fotos.size,
                    GlobalVariables.fotos[GlobalVariables.fotos.size - 1]
                )

                for (i in (GlobalVariables.fotos.size - 2)..0) {
                    if (i >= 1)
                        GlobalVariables.fotos[i] = GlobalVariables.fotos[i - 1]
                }

                GlobalVariables.fotos[0] = imagen
            }
            else
            {
                for (i in (GlobalVariables.fotos.size - 1)..0) {
                    if (i >= 1)
                        GlobalVariables.fotos[i] = GlobalVariables.fotos[i - 1]
                }
                GlobalVariables.fotos[0] = imagen
            }


        }


        if(GlobalVariables.fotos.size > 0)
        {
            binding.ultimaFoto.setImageBitmap(GlobalVariables.fotos[0])
        }
    }


    fun pidePermisos()
    {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        requireActivity().startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}