package com.example.myapplication.ui.consulta

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examenkotlin27032024.Model.ProductData
import com.example.examenkotlin27032024.Repository.CharactersRepository
import com.google.gson.Gson
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ConsultaViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    private val repository = CharactersRepository()

    val getProductResponse = MutableLiveData<ProductData>()


    fun getProduct(
        idProducto: Int
    ) = viewModelScope.launch {
        repository.GetProduct(
            idProducto = idProducto
        ).collectLatest { repositoryResponse ->


            val gson1 = Gson()
            val json1 = gson1.toJson(repositoryResponse)
            Log.d("EXAMEN", "GetProducto response: $json1")

            getProductResponse.postValue(repositoryResponse!!)

        }
    }
}