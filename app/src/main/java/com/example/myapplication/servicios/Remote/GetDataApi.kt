package com.example.examenkotlin27032024.Remote

import com.example.examenkotlin27032024.Model.GetProductsResponse
import com.example.examenkotlin27032024.Model.ProductData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface GetDataApi {


    @GET
    suspend fun getProductos(
            @Url url: String
    ): Response<GetProductsResponse>


    @GET
    suspend fun getProducto(
        @Url url: String
    ): Response<ProductData>

}