package com.example.examenkotlin27032024.Model

data class GetProductsResponse (

    val results: List<ProductData> = listOf()

)