package com.example.examenkotlin27032024.Model


data class ProductData(

    var id: Int,
    var title: String,
    var price: Double,
    var description: String,
    var category: String,
    var image:  String,
    var rating: RatingData

)